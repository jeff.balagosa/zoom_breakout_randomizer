# Zoom Breakout Room Randomizer

## Description
The `zoom_breakout_randomizer.py` script is a simple utility for selecting a random Zoom breakout room. It takes the total number of breakout rooms as input and returns a randomly selected room number, along with a friendly reminder.

## Usage
You can run the script by passing the total number of rooms as a command-line argument, or you can run the script without any arguments, and it will prompt you for the total number of rooms.

### Command-line Argument
Example: If you have 10 breakout rooms, you can run the script as follows:
```bash
python zoom_breakout_randomizer.py 10
```

### Interactive Prompt
If you run the script without any arguments, it will prompt you to enter the total number of rooms.
```bash
python zoom_breakout_randomizer.py
```

## Requirements
- Python 3.x

### MIT License
---

Copyright (c) 2023 Jeff Balagosa

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

# import random to help randomize the room selection (Documentation: https://docs.python.org/3.10/library/random.html)
import random
# import sys to gain the ability to pass in a param when running the script (Documentation: https://docs.python.org/3.10/library/sys.html)
import sys

def select_room(total_rooms):
    room = random.randint(1, total_rooms)
    return f"Please join room {room}. If it is empty, please re-run the script. \nRemember: The connections you make here are just as important as the knowledge you gain."

# check if param is entered. If not, ask for input
if len(sys.argv) > 1:
    get_total_rooms = int(sys.argv[1])
else:
    get_total_rooms = int(input("How many rooms are there? "))

print(select_room(get_total_rooms))
